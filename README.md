# Ubuntu Docker Image with sudo

## Info

|param|value|
|:-:|:-:|
|default user|ubuntu|
|defailt password|ubuntu|
|admin|true|
|$HOME|/home/ubuntu|
|$SHELL|bash|

## Quick Start

```shell
git clone https://gitlab.com/Hirochan/ubuntu-docker.git

cd ubuntu-docker

sudo docker build -t myubuntu .

sudo docker run -it --name Linux myubuntu
```

## Restart Interactive Mode

```shell
docker start -i Linux
```

## Additional Packages

|package name|
|:-:|
|sudo|
|tar|
|unzip|
