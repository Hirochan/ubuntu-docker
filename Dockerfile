FROM ubuntu:latest

ARG UID=1000
ARG USER=ubuntu
ARG PASSWD=ubuntu

RUN apt update && apt upgrade -y 
RUN apt install -y sudo tar unzip

RUN useradd -m --uid ${UID} --groups sudo ${USER} \
    && echo ${USER}:${PASSWD} | chpasswd

USER ubuntu
WORKDIR /home/${USER}
